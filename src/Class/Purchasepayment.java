
package Class;

public class Purchasepayment {
    
    public int paymentid;
    public int supplierid;
    public String paymenttype;
    public int chequeno;
    public String date;
    public double amountpaid;
    public double balance;

    public Purchasepayment(int paymentid, int supplierid, String paymenttype, int chequeno, String date, double amountpaid, double balance) {
        this.paymentid = paymentid;
        this.supplierid = supplierid;
        this.paymenttype = paymenttype;
        this.chequeno = chequeno;
        this.date = date;
        this.amountpaid = amountpaid;
        this.balance = balance;
    }
    
    public Purchasepayment() {}

    public int getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(int paymentid) {
        this.paymentid = paymentid;
    }

    public int getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(int supplierid) {
        this.supplierid = supplierid;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public int getChequeno() {
        return chequeno;
    }

    public void setChequeno(int chequeno) {
        this.chequeno = chequeno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getAmountpaid() {
        return amountpaid;
    }

    public void setAmountpaid(double amountpaid) {
        this.amountpaid = amountpaid;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }


}