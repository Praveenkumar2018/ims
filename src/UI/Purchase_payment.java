package UI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Class.ConnectionProvider;
import Class.Purchasepayment;
import javax.swing.table.TableModel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Purchase_payment extends javax.swing.JFrame {


    public Purchase_payment() {
        initComponents();
        generatepaymentid();
        setdate();
        loadSupplierID();
        
        paymentid.setEditable(false);
        paypurchaseid.setEditable(false);
        paymentdate.setEditable(false);
        balance.setEditable(false);
        cheque.setText("000000");
        cheque.setEditable(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        amountpaid = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        balance = new javax.swing.JTextField();
        paymenttypecombo = new javax.swing.JComboBox<>();
        cheque = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        paymentid = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        paymentdate = new javax.swing.JTextField();
        paymentinsert = new javax.swing.JButton();
        payclearfeild = new javax.swing.JButton();
        paypurchaseid = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        psearchcombo = new javax.swing.JComboBox<>();
        search = new javax.swing.JButton();
        psearchtext = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ppaymenttable = new javax.swing.JTable();
        oviewall1 = new javax.swing.JButton();
        tclearall1 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        totalamount = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        arrears = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(1315, 681));

        jLabel1.setBackground(new java.awt.Color(0, 31, 63));
        jLabel1.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Purchase Payment");
        jLabel1.setOpaque(true);

        jPanel2.setBackground(new java.awt.Color(245, 245, 245));
        jPanel2.setPreferredSize(new java.awt.Dimension(272, 502));

        jLabel13.setFont(new java.awt.Font("Calibri Light", 0, 14)); // NOI18N
        jLabel13.setText("Payment Date");

        amountpaid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                amountpaidKeyReleased(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Calibri Light", 0, 14)); // NOI18N
        jLabel9.setText("Amount Paid");

        jLabel12.setFont(new java.awt.Font("Calibri Light", 0, 14)); // NOI18N
        jLabel12.setText("Balance");

        paymenttypecombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Select--", "Cash", "Cheque", " " }));
        paymenttypecombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paymenttypecomboActionPerformed(evt);
            }
        });

        cheque.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                chequeKeyReleased(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Calibri Light", 0, 14)); // NOI18N
        jLabel11.setText("Supplier ID");

        jLabel10.setFont(new java.awt.Font("Calibri Light", 0, 14)); // NOI18N
        jLabel10.setText("Payment ID");

        jLabel8.setFont(new java.awt.Font("Calibri Light", 0, 14)); // NOI18N
        jLabel8.setText("Payment Type");

        jLabel15.setFont(new java.awt.Font("Calibri Light", 0, 14)); // NOI18N
        jLabel15.setText("Cheque Number");

        paymentdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paymentdateActionPerformed(evt);
            }
        });

        paymentinsert.setBackground(new java.awt.Color(0, 31, 63));
        paymentinsert.setFont(new java.awt.Font("Calibri Light", 0, 36)); // NOI18N
        paymentinsert.setForeground(new java.awt.Color(0, 31, 63));
        paymentinsert.setText("Proceed Payment");
        paymentinsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paymentinsertActionPerformed(evt);
            }
        });

        payclearfeild.setBackground(new java.awt.Color(0, 158, 113));
        payclearfeild.setFont(new java.awt.Font("Calibri Light", 0, 36)); // NOI18N
        payclearfeild.setForeground(new java.awt.Color(0, 31, 63));
        payclearfeild.setText("Clear All");
        payclearfeild.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                payclearfeildActionPerformed(evt);
            }
        });

        paypurchaseid.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Select  ID--" }));
        paypurchaseid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paypurchaseidActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(paymenttypecombo, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(cheque, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(paymentinsert, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(payclearfeild, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(paypurchaseid, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(paymentid, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(paymentdate, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(amountpaid, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(10, 10, 10)
                            .addComponent(balance))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(paymentid, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(paypurchaseid, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(paymenttypecombo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cheque, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(paymentdate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(amountpaid, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(balance, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(116, 116, 116)
                .addComponent(paymentinsert, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(payclearfeild, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(99, Short.MAX_VALUE))
        );

        psearchcombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Select--", "Payment ID", "Supplier ID", " " }));
        psearchcombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                psearchcomboActionPerformed(evt);
            }
        });

        search.setBackground(new java.awt.Color(255, 255, 255));
        search.setText("Search");
        search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(psearchcombo, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(psearchtext, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(psearchcombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(search)
                .addComponent(psearchtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel4.setBackground(new java.awt.Color(245, 245, 245));
        jPanel4.setPreferredSize(new java.awt.Dimension(886, 431));

        ppaymenttable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Payment ID", "Supplier ID", "Payment Type", "Date", "Cheque Number", "Amount paid", "Balance"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        ppaymenttable.setPreferredSize(new java.awt.Dimension(675, 192));
        ppaymenttable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ppaymenttableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ppaymenttable);
        if (ppaymenttable.getColumnModel().getColumnCount() > 0) {
            ppaymenttable.getColumnModel().getColumn(0).setResizable(false);
            ppaymenttable.getColumnModel().getColumn(1).setResizable(false);
            ppaymenttable.getColumnModel().getColumn(2).setResizable(false);
            ppaymenttable.getColumnModel().getColumn(3).setResizable(false);
            ppaymenttable.getColumnModel().getColumn(4).setResizable(false);
            ppaymenttable.getColumnModel().getColumn(5).setResizable(false);
            ppaymenttable.getColumnModel().getColumn(6).setResizable(false);
        }

        oviewall1.setBackground(new java.awt.Color(0, 158, 113));
        oviewall1.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        oviewall1.setForeground(new java.awt.Color(0, 31, 63));
        oviewall1.setText("View All");
        oviewall1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oviewall1ActionPerformed(evt);
            }
        });

        tclearall1.setBackground(new java.awt.Color(220, 20, 60));
        tclearall1.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        tclearall1.setForeground(new java.awt.Color(0, 31, 63));
        tclearall1.setText("Clear All");
        tclearall1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tclearall1ActionPerformed(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(245, 245, 245));

        jLabel14.setBackground(new java.awt.Color(0, 31, 63));
        jLabel14.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Total Amount :");
        jLabel14.setOpaque(true);

        totalamount.setBackground(new java.awt.Color(0, 31, 63));
        totalamount.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        totalamount.setForeground(new java.awt.Color(255, 255, 255));
        totalamount.setOpaque(true);

        jLabel3.setBackground(new java.awt.Color(0, 31, 63));
        jLabel3.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Arrears :");
        jLabel3.setOpaque(true);

        arrears.setBackground(new java.awt.Color(0, 31, 63));
        arrears.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        arrears.setForeground(new java.awt.Color(255, 255, 255));
        arrears.setOpaque(true);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(totalamount, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(arrears, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(totalamount, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(arrears, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/customer.png"))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(tclearall1, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(oviewall1, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(oviewall1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tclearall1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 866, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 1156, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 633, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 633, Short.MAX_VALUE))
                .addContainerGap())
        );

        jMenu1.setText("Home");
        jMenu1.addMenuKeyListener(new javax.swing.event.MenuKeyListener() {
            public void menuKeyPressed(javax.swing.event.MenuKeyEvent evt) {
                jMenu1MenuKeyPressed(evt);
            }
            public void menuKeyReleased(javax.swing.event.MenuKeyEvent evt) {
            }
            public void menuKeyTyped(javax.swing.event.MenuKeyEvent evt) {
            }
        });

        jMenu5.setText("Home");
        jMenu5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu5MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu5);

        jMenu3.setText("Return to Dashboard");
        jMenu3.addMenuKeyListener(new javax.swing.event.MenuKeyListener() {
            public void menuKeyPressed(javax.swing.event.MenuKeyEvent evt) {
                jMenu3MenuKeyPressed(evt);
            }
            public void menuKeyReleased(javax.swing.event.MenuKeyEvent evt) {
            }
            public void menuKeyTyped(javax.swing.event.MenuKeyEvent evt) {
            }
        });
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenu4.setText("Return Purchase Interface");
        jMenu4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu4MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu4);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Report");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1505, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 697, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    double tt,bal;
    
    boolean cn_val;
    boolean ap_val;
  
    //generate payment id
    public void generatepaymentid(){
      Connection connectionpid = ConnectionProvider.getInstance().getDBConnection();
        try {
           Statement stpid = connectionpid.createStatement();
           ResultSet rspid = stpid.executeQuery("select paymentid from payments");
           while(rspid.next())
           {
              int pid = 0;
              pid = rspid.getInt("paymentid") + 1;
              paymentid.setText(String.valueOf(pid));
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
    }           
    
    public void loadbalance(){
    
         try
        {
            Connection con = ConnectionProvider.getInstance().getDBConnection();
            Statement st = con.createStatement();
            String query = "SELECT SUM(balance) FROM payments WHERE supplierid='"+paypurchaseid.getSelectedItem()+"' GROUP BY supplierid";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next())
            {
               bal = Double.parseDouble(rs.getString("SUM(balance)"));
               arrears.setText(String.valueOf(bal));
            }
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Error occured, "+ex,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void loadtotal()
    {
        try
        {
            Connection con = ConnectionProvider.getInstance().getDBConnection();
            Statement st = con.createStatement();
            String query = "SELECT SUM(totalamount) FROM purchase WHERE supplierid='"+paypurchaseid.getSelectedItem()+"' GROUP BY supplierid";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next())
            {
               tt = Double.parseDouble(rs.getString("SUM(totalamount)"));
               totalamount.setText(String.valueOf(tt));
            }
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Error occured, "+ex,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }        

    
    //set outwards registered date
    public void setdate(){
        Calendar cal = Calendar.getInstance();
        int d = cal.get(Calendar.DATE);
        int m = cal.get(Calendar.MONTH);
        int y = cal.get(Calendar.YEAR);
        paymentdate.setText(String.valueOf(y+"-"+m+"-"+d));
    }
    
    //Balance Calculation
    public void calbalance(){

        double paid = Double.parseDouble(amountpaid.getText());
        double paybal = tt - paid;
        balance.setText(String.valueOf(paybal));
    
    }
    
    //Amounty paid action
    private void amountpaidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_amountpaidKeyReleased
        String osid = amountpaid.getText();
        String PATTERN = "^[0-9]+$";
        Pattern pat = Pattern.compile(PATTERN);
        Matcher match = pat.matcher(osid);

        cn_val = !match.matches();

        if (ap_val) {
            JOptionPane.showMessageDialog(null, "Insert Numbers Only" );
        }
        calbalance();
    }//GEN-LAST:event_amountpaidKeyReleased

    
    //payment type selection
    String paycombo;
    private void paymenttypecomboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paymenttypecomboActionPerformed
        paycombo = String.valueOf(paymenttypecombo.getSelectedItem());
        
            if(paycombo.equals("Cheque")){
            
                cheque.setEditable(true);
            
            }
    }//GEN-LAST:event_paymenttypecomboActionPerformed

    private void psearchcomboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_psearchcomboActionPerformed

    }//GEN-LAST:event_psearchcomboActionPerformed

    private void searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchActionPerformed
        clear_jtable();
        purchasepayment_search();
        purchasepayment_search_table();
    }//GEN-LAST:event_searchActionPerformed

    
    private void oviewall1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oviewall1ActionPerformed
        clear_jtable();
        purchasepayment_ViewAll();
        purchasepayment_viewall_table();
    }//GEN-LAST:event_oviewall1ActionPerformed

    private void tclearall1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tclearall1ActionPerformed
        clear_jtable();
    }//GEN-LAST:event_tclearall1ActionPerformed

    private void jMenu3MenuKeyPressed(javax.swing.event.MenuKeyEvent evt) {//GEN-FIRST:event_jMenu3MenuKeyPressed
        
    }//GEN-LAST:event_jMenu3MenuKeyPressed

    private void jMenu1MenuKeyPressed(javax.swing.event.MenuKeyEvent evt) {//GEN-FIRST:event_jMenu1MenuKeyPressed
    }//GEN-LAST:event_jMenu1MenuKeyPressed

    private void paypurchaseidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paypurchaseidActionPerformed
        loadtotal();
        loadbalance();
    }//GEN-LAST:event_paypurchaseidActionPerformed

    private void paymentdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paymentdateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_paymentdateActionPerformed

    private void paymentinsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paymentinsertActionPerformed
        String query = "INSERT INTO payments(paymentid,supplierid,paymenttype,chequeno,date,amountpaid,balance) VALUES ('"+Integer.parseInt(paymentid.getText())+"','"+paypurchaseid.getSelectedItem()+"','"+paymenttypecombo.getSelectedItem()+"','"+Integer.parseInt(cheque.getText())+"','"+paymentdate.getText()+"','"+Double.parseDouble(amountpaid.getText())+"','"+Double.parseDouble(balance.getText())+"')";
        executequery(query, "Inserted");
        clear();
        generatepaymentid();
        setdate();
        cheque.setText("000000");
        purchasepayment_ViewAll();
        purchasepayment_viewall_table();
    }//GEN-LAST:event_paymentinsertActionPerformed

    private void payclearfeildActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_payclearfeildActionPerformed
       clear();
       generatepaymentid();
       setdate();
       cheque.setText("000000");
    }//GEN-LAST:event_payclearfeildActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        this.setVisible(false);
        new Purchase_dashboard().setVisible(true);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void jMenu4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu4MouseClicked
        this.dispose();
        new Purchase_interface().setVisible(true);
    }//GEN-LAST:event_jMenu4MouseClicked

    private void ppaymenttableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ppaymenttableMouseClicked
    }//GEN-LAST:event_ppaymenttableMouseClicked

    private void jMenu5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu5MouseClicked
        this.dispose();
        new Dashboard().setVisible(true);
    }//GEN-LAST:event_jMenu5MouseClicked

    private void chequeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chequeKeyReleased
        String osid = cheque.getText();
        String PATTERN = "^[0-9]{6}+$";
        Pattern pat = Pattern.compile(PATTERN);
        Matcher match = pat.matcher(osid);

        cn_val = !match.matches();

        if (cn_val) {
            JOptionPane.showMessageDialog(null, "Insert Only six Numbers" );
        } 

    }//GEN-LAST:event_chequeKeyReleased

    public void clear(){
        cheque.setText("");
        paymentdate.setText("");
        totalamount.setText("");
        amountpaid.setText("");
        balance.setText("");
    }
     //execute query function
     public void executequery(String query, String message){
       Connection con = ConnectionProvider.getInstance().getDBConnection();
       Statement st;
       try{
           st = con.createStatement();
           if((st.executeUpdate(query)) == 1)
           {
               DefaultTableModel model = (DefaultTableModel)ppaymenttable.getModel();
               model.setRowCount(0);
               purchasepayment_ViewAll(); 
               JOptionPane.showMessageDialog(null, "Data " + message + " Successfully");
           }else{
               JOptionPane.showMessageDialog(null, "Data Not" + message);
           }
       }catch(Exception ex){
           ex.printStackTrace();
       }
   }
      private void loadSupplierID()
    {
        try
        {
            Connection con = ConnectionProvider.getInstance().getDBConnection();
            String query = "SELECT * FROM purchase";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            
            
            while(rs.next())
            {
                paypurchaseid.addItem(String.valueOf(rs.getInt("supplierid")));
            }
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Error occured, "+ex,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    public void clear_jtable(){
      
         DefaultTableModel model = (DefaultTableModel)ppaymenttable.getModel();
         model.setRowCount(0);
       }
    
    
    public ArrayList<Purchasepayment> purchasepayment_ViewAll()
   {
       ArrayList<Purchasepayment> paymentlist = new ArrayList<Purchasepayment>();
       Connection connection = ConnectionProvider.getInstance().getDBConnection();
      
       String query = "SELECT * FROM  payments";
        
       try {
          
           Statement st = connection.createStatement();
           ResultSet rs = st.executeQuery(query);
           Purchasepayment payment;
           while(rs.next())
           {
               payment = new Purchasepayment(rs.getInt("paymentid"),rs.getInt("supplierid"),rs.getString("paymenttype"),rs.getInt("chequeno"),rs.getString("date"),rs.getDouble("amountpaid"),rs.getDouble("balance"));
               paymentlist.add(payment);
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return paymentlist;
   }
    
     public void purchasepayment_viewall_table(){
       ArrayList<Purchasepayment> list = purchasepayment_ViewAll();
       DefaultTableModel model = (DefaultTableModel)ppaymenttable.getModel();
       Object[] row = new Object[8];
       for(int i = 0; i < list.size(); i++)
       {
           row[0] = list.get(i).getPaymentid();
           row[1] = list.get(i).getSupplierid();
           row[2] = list.get(i).getPaymenttype();
           row[3] = list.get(i).getDate();
           row[4] = list.get(i).getChequeno();
           row[5] = list.get(i).getAmountpaid();
           row[6] = list.get(i).getBalance();

           model.addRow(row);
       }
    }

     
     //search
       public ArrayList<Purchasepayment> purchasepayment_search()
   {
       ArrayList<Purchasepayment> purchasepaymentps = new ArrayList<Purchasepayment>();
       Connection connectionps = ConnectionProvider.getInstance().getDBConnection();
      
       String psc = String.valueOf(psearchcombo.getSelectedItem());
       int pst = Integer.parseInt(psearchtext.getText());
       String spst = psearchtext.getText();
       String query;
       
       if(psc.equals("Payment ID")){
       query = "SELECT * FROM  payments where paymentid ='"+pst+"'";
       }else if(psc.equals("Supplier ID")){
       query = "SELECT * FROM  payments where purchaseid ='"+pst+"'";
       }else  query =  "SELECT * FROM  payments"; 
       try {

           Statement stps  = connectionps .createStatement();
           ResultSet rs  = stps .executeQuery(query);
           Purchasepayment payment;
           while(rs .next())
           {
               payment = new Purchasepayment(rs.getInt("paymentid"),rs.getInt("supplierid"),rs.getString("paymenttype"),rs.getInt("chequeno"),rs.getString("date"),rs.getDouble("amountpaid"),rs.getDouble("balance"));
               purchasepaymentps.add(payment);
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return purchasepaymentps ;
   }
       
     public void purchasepayment_search_table(){
       ArrayList<Purchasepayment> list = purchasepayment_search();
       DefaultTableModel model = (DefaultTableModel)ppaymenttable.getModel();
       Object[] row = new Object[8];
       for(int i = 0; i < list.size(); i++)
       {
           row[0] = list.get(i).getPaymentid();
           row[1] = list.get(i).getSupplierid();
           row[2] = list.get(i).getPaymenttype();
           row[3] = list.get(i).getDate();
           row[4] = list.get(i).getChequeno();
           row[5] = list.get(i).getAmountpaid();
           row[6] = list.get(i).getBalance();

           model.addRow(row);
       }
    }
     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Purchase_payment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Purchase_payment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Purchase_payment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Purchase_payment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Purchase_payment().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField amountpaid;
    private javax.swing.JLabel arrears;
    private javax.swing.JTextField balance;
    private javax.swing.JTextField cheque;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton oviewall1;
    private javax.swing.JButton payclearfeild;
    private javax.swing.JTextField paymentdate;
    private javax.swing.JTextField paymentid;
    private javax.swing.JButton paymentinsert;
    private javax.swing.JComboBox<String> paymenttypecombo;
    private javax.swing.JComboBox<String> paypurchaseid;
    private javax.swing.JTable ppaymenttable;
    private javax.swing.JComboBox<String> psearchcombo;
    private javax.swing.JTextField psearchtext;
    private javax.swing.JButton search;
    private javax.swing.JButton tclearall1;
    private javax.swing.JLabel totalamount;
    // End of variables declaration//GEN-END:variables
}





